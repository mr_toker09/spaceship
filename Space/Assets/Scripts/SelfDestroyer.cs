﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestroyer : MonoBehaviour
{
    public float delay;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DestroyDust());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator DestroyDust()
    {
        yield return new WaitForSeconds(delay);
        Destroy(this.gameObject);
    }
}
