﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipManeuverController : MonoBehaviour
{
    #region Global variables
    public float maxVelocity = 3;
    public float rotationSpeed = 3;
    public SpriteRenderer spriteRnd;
    public GameObject[] trails;

    public GameObject crashFX;
    public float crashDuration;
    public float crashMagnitude;
    #endregion

    #region Local variables
    private Rigidbody2D _rigidBody;

    private float yAxis;
    private float xAxis;

    private bool isDead = false;

    private CameraMovement camMove;
    private CameraShake camShake;
    #endregion

    private void OnValidate()
    {
        _rigidBody = this.GetComponent<Rigidbody2D>();
        spriteRnd = this.gameObject.GetComponent<SpriteRenderer>();

        camMove = FindObjectOfType<CameraMovement>();
        camShake = FindObjectOfType<CameraShake>();
    }

    // Start is called before the first frame update
    void Start()
    {
        camMove.enabled = true;
        camShake.enabled = false;

        isDead = false;
    }

    void Update()
    {
        if (!isDead)
        {
            yAxis = Input.GetAxis("Vertical");
            xAxis = Input.GetAxis("Horizontal");

            Rotate(this.transform, xAxis * -rotationSpeed);
        }
        else
        {
            _rigidBody.velocity = Vector2.zero;
        }
    }

    void FixedUpdate()
    {
        ThrustForward(yAxis);   
    }

    void ClampVelocity()
    {
        float x = Mathf.Clamp(_rigidBody.velocity.x, 0, maxVelocity);
        float y = Mathf.Clamp(_rigidBody.velocity.y, 0, maxVelocity);

        _rigidBody.velocity = new Vector2(x, y);
    }

    void ThrustForward(float amount)
    {
        Vector2 force = transform.up * amount;
        _rigidBody.AddForce(force);
    }

    void Rotate(Transform t, float amount)
    {
        t.Rotate(0, 0, amount);
    }


    public void OnCrash()
    {
        if (!isDead)
        {
            camMove.enabled = false;
            camShake.enabled = true;

            isDead = true;

            Instantiate(crashFX, this.transform.position, this.transform.rotation);

            //StartCoroutine(camShake.Shake(crashDuration, crashMagnitude));

            for (int i = 0; i < trails.Length; i++)
            {
                trails[i].SetActive(false);
            }
            spriteRnd.enabled = false;
        }
    }
}
