﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidController : MonoBehaviour
{
    public float minSpeed;
    public float maxSpeed;

    public float minScale;
    public float maxScale;

    public float rotationSpeed;
    public float size;

    private ShipManeuverController spaceShip;

    private void OnValidate()
    {
        spaceShip = FindObjectOfType<ShipManeuverController>();
    }

    // Start is called before the first frame update
    void Start()
    {
        rotationSpeed = UnityEngine.Random.Range(minSpeed, maxSpeed);
        size = UnityEngine.Random.Range(minScale, maxScale);
        this.transform.localScale = new Vector3(size, size);
    }

    // Update is called once per frame
    void Update()
    {
        Rotate();
    }

    void Rotate()
    {
        transform.Rotate(Vector3.forward * rotationSpeed);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            spaceShip.OnCrash();
        }
    }
}
