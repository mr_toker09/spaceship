﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour
{
    public SpriteRenderer spriteRnd;
    public GameObject[] trails;

    public GameObject crashFX;
    public float crashDuration;
    public float crashMagnitude;

    private CameraMovement camMove;
    private CameraShake camShake;

    private void OnValidate()
    {
        spriteRnd = this.gameObject.GetComponent<SpriteRenderer>();
        camMove = FindObjectOfType<CameraMovement>();
        camShake = FindObjectOfType<CameraShake>();
    }
    // Start is called before the first frame update
    void Start()
    {
        camMove.enabled = true;
        camShake.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnCrash()
    {
        camMove.enabled = false;
        camShake.enabled = true;

        Instantiate(crashFX, this.transform.position, this.transform.rotation);

        StartCoroutine(camShake.Shake(crashDuration, crashMagnitude));

        for (int i = 0; i < trails.Length; i++)
        {
            trails[i].SetActive(false);
        }
        spriteRnd.enabled = false;
    }
}
